package it.unicas;
import java.util.Scanner;
public class Shell {
    String command="";
    Solve s=new Solve();
    public void showShell(){
        Scanner in=new Scanner(System.in);
        while (command.length()==0) {
            System.out.println("Command?: ");
            command = in.nextLine();
            if (command.length()!=0){
                s.SolveCommand(command);
                command="";
            }
        }

        return;
    }
}
