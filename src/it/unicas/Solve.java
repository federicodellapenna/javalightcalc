package it.unicas;

import java.util.Scanner;
import java.lang.Math;

public class Solve {
    float a,b,r,ans;
    public void SolveCommand(String c){
        int i=-1;
        i=c.indexOf('h'); //routine dell'help - OK
        if (i>=0){
            System.out.println("|Help|\n");
            System.out.println("For now, this small software writes only strings in 'answer' to simple commands.");
            System.out.println("You can, for example, write a (simple) expression with a math symbol (+,*,-,/).\n");
            i=-1;
        }
        i=c.indexOf('q'); //routine di uscita
        if (i>=0){
            Scanner quit=new Scanner(System.in);
            String sq;
            System.out.println("|Quit|\n");
            System.out.println("Enter capital Y to quit: ");
            sq=quit.nextLine();
            if(sq=="Y"){

            }
            i=-1;
        }
        i=c.indexOf('+'); //routine dell'addizione - funzionante
        if (i>0){
            String sub,sub2;
            Result res=new Result();
            sub=c.substring(0,i);
            a=Float.parseFloat(sub);
            sub2=c.substring(i+1,c.length());
            b=Float.parseFloat(sub2);
            r=a+b;
            ans=r;
            res.showResult(ans);
            i=-1;
        }
        i=c.indexOf('-');
        if (i>0){
            String sub,sub2;
            Result res=new Result();
            sub=c.substring(0,i);
            a=Float.parseFloat(sub);
            sub2=c.substring(i+1,c.length());
            b=Float.parseFloat(sub2);
            r=a-b;
            ans=r;
            res.showResult(ans);
            i=-1;
        }
        i=c.indexOf('*');
        if (i>0){
            String sub,sub2;
            Result res=new Result();
            sub=c.substring(0,i);
            a=Float.parseFloat(sub);
            sub2=c.substring(i+1,c.length());
            b=Float.parseFloat(sub2);
            r=a*b;
            ans=r;
            res.showResult(ans);
            i=-1;
        }
        i=c.indexOf('/');
        if (i>0){
            String sub,sub2;
            Result res=new Result();
            sub=c.substring(0,i);
            a=Float.parseFloat(sub);
            sub2=c.substring(i+1,c.length());
            b=Float.parseFloat(sub2);
            r=a/b;
            ans=r;
            res.showResult(ans);
            i=-1;
        }
        i=c.indexOf("sin");
        if (i>=0){
            String sub,sub2;
            Result res=new Result();
            sub=c.substring(2,c.length());
            a=Float.parseFloat(sub);
            b=a;
            //r=Math.sin(b);
            ans=r;
            res.showResult(ans);
            i=-1;
        }
        i=c.indexOf("cos");
        if (i>=0){
            System.out.println("coseno");
            i=-1;
        }
        i=c.indexOf("tan");
        if (i>=0){
            System.out.println("tangente");
            i=-1;
        }
    }

}
